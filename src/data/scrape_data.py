# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path

# from dotenv import find_dotenv, load_dotenv


import click
import pandas as pd
import requests as rq
import re

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


def retrieve_cookies():
    logging.info('Retrieving cookies')

    login_headers = {
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36",
    }

    rs = rq.get('https://www.trendhunter.com/login', headers=login_headers)
    token = re.search('TH_Token = \"([^\"]*)\"', rs.text).group(1)

    logging.info('Trying to log in')

    data = f"act=login&username=vakiv98498@mnsaf.com&password=vakiv98498@mnsaf.comvakiv98498@mnsaf.com&remember_me=0&tk={token}&aj=1^"
    rs = rq.post('https://www.trendhunter.com/apps/login', data=data, headers=login_headers)

    cookies = dict(rs.cookies)
    cookie_str = f"uniqueid={cookies['uniqueid']}; exp_anon=1; TrendhunterPopup=1; exp_expiration={cookies['exp_expiration']}; exp_sessionid={cookies['exp_sessionid']}"

    logging.info('Successfully logged in')
    return cookie_str


def parse_data(cookies, pages, output):
    logging.info(f'Scaping {pages} pages')

    scape_headers = {
        "cookie": cookies,
        "referer": "https://www.trendhunter.com/apps/dashboard",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36",
    }
    results = []
    for i in range(pages):
        rs = rq.get(
            f'https://www.trendhunter.com/apps/dashboard?act=load-m&p=category&v=0&pg={i+1}&t=ideas&aj=true',
            headers=scape_headers,
        )
        results += rs.json()['data']['articles']
        logging.info(f'Page #{i:<4} | {rs.status_code}')

    pd.DataFrame(results).to_parquet(output, index=False)
    logging.info(f'Saved to {output}')


@click.command()
@click.option('--pages', default=100, help='Number of pages to scrape')
@click.option('--output', default='trendhunter.parquet', help='Path to the output Parquet file')
def scrape_trendhunter(pages, output):
    cookies = retrieve_cookies()
    parse_data(cookies, pages, output)


if __name__ == '__main__':
    scrape_trendhunter()
