import click
import logging

import pandas as pd

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


@click.command()
@click.option('--input-file', '-i', prompt='Input file name', help='Path to the input file')
@click.option('--output-file', '-o', prompt='Output file name', help='Path to the output file')
def process_features(input_file, output_file):
    '''
    Feature preprocessing: 
    - boolean cols
    - datetime col
    - text col
    '''
    logging.info(f'Reading {input_file}')

    df = pd.read_parquet(input_file)

    logging.info('Processing countries columns')

    countries_cols = ['africa', 'asia', 'europe', 'north_america', 'south_america']
    df[countries_cols] = df[countries_cols].fillna(0).astype(int)

    logging.info('Processing age columns')

    age_cols = ['demo_0_2', 'demo_under_12', 'demo_12_18', 'demo_18_35', 'demo_35_55', 'demo_over_55']
    df[age_cols] = df[age_cols].fillna(0).astype(int)

    logging.info('Processing text columns')

    df['title'] = df['title1']

    df['date'] = df['added_date']

    logging.info('Subsampling columns')

    df = df[['title', 'date', 'activity', 'popularity'] + countries_cols + age_cols]

    logging.info(f'Writing {output_file}')

    df.to_parquet(output_file, index=False)


if __name__ == '__main__':
    process_features()
