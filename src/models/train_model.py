import click
import logging
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
import joblib

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


@click.command()
@click.option('--train_data', '-d', type=str, required=True, help='Path to the CSV training data file')
@click.option('--target_col', '-t', type=str, required=True, help='Name of the target column')
@click.option('--text_col', '-x', type=str, required=True, help='Name of the text column')
@click.option('--n_estimators', '-n', type=int, default=100, help='Number of trees in the forest')
@click.option('--max_depth', '-m', type=int, default=None, help='Maximum depth of the trees')
@click.option('--random_state', '-r', type=int, default=42, help='Random state for reproducibility')
@click.option('--model_path', '-p', type=str, default='model.joblib', help='Path to save the trained model')
def train_model(train_data, target_col, text_col, n_estimators, max_depth, random_state, model_path):
    """
    Train a Random Forest Regression model on the provided training data, including text data.
    """
    logging.info('Loading data...')
    train_df = pd.read_parquet(train_data)

    logging.info('Preparing data...')
    numerical_cols = [col for col in train_df.columns if col not in [target_col, text_col]]
    X = train_df[numerical_cols + [text_col]]
    y = train_df[target_col]

    logging.info('Creating pipeline...')
    numeric_transformer = 'passthrough'
    text_transformer = TfidfVectorizer()

    preprocessor = ColumnTransformer(
        transformers=[('num', numeric_transformer, numerical_cols), ('txt', text_transformer, [text_col])]
    )

    model = RandomForestRegressor(n_estimators=n_estimators, max_depth=max_depth, random_state=random_state)
    pipeline = Pipeline([('preprocessor', preprocessor), ('regressor', model)])

    logging.info(X.shape, y.shape)

    logging.info('Training model')
    pipeline.fit(X, y)

    logging.info(f'Saving model to {model_path}')
    joblib.dump(pipeline, model_path)

    logging.info('Training completed successfully.')


if __name__ == '__main__':
    train_model()
