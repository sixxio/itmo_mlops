from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Prediction product/idea score by its description and metadata.',
    author='Nikita Safonov (sixxio)',
    license='',
)
